<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $incrementing = false;
    protected $primaryKey = null;
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}

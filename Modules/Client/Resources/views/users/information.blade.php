@extends('client::layouts.master')

@section('content')
    <section class="roles-and-permissions">
        <div class="container">
            @include('admin::layouts.partials._success')

            <section class="heading">
                <div class="container">
                    <div class="heading__body old-requests">
                        <h1>  {{ __('معلومات عن المبانى والايجارات') }}</h1>
                        <span class="line"></span>
                    </div>
                </div>
            </section>

       <div class="search-results_details" id="advance_added">
           <div class="row">
               <div class="col-md-9">
                   <div class="search_info">
                       @inject("informations","\App\OrderInformation")
                       <div class="row">
                           <div class="col-md-12">
                               <div class="search-results__action">
                                   <!-- Button trigger modal -->
                                       <div class="form-group">
                                           @if($informations->get()->isEmpty())
                                           <button type="button" class="btn btn-primary" id="orderButton" style="display: block;" data-toggle="modal" data-target="#exampleModal">
                                               طلب الحصول على المعلومات
                                           </button>
                                    @else
                                               @if(!$informations->where('is_accepted',true)->get()->isNotEmpty())

                                               <div class="alert alert-warning" role="alert" id="alertOrder" >
                                               تم تقديم الطلب وفى حالة موافقة الاداره سيظهر لك الملف
                                           </div>
                                               @else
                                                   <div class="alert alert-success" role="alert" id="alertOrder" >
                                                       تم  موافقة الاداره سيظهر لك الملف
                                                   </div>
                                           @endif
                                           @endif
                                               <div class="alert alert-warning" role="alert" id="alertOrder" style="display: none ;">
                                                   تم تقديم الطلب وفى حالة موافقة الاداره سيظهر لك الملف
                                               </div>

                                       </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
            @if($informations->where('is_accepted',true)->get()->isNotEmpty())
   <div class="search-results_details">
       <div class="row">
           <div class="col-md-9">
               <div class="search_info">
                   @include('client::users.resultInformation')
               </div>
           </div>
       </div>
   </div>
    @endif
</div>
</section>


@endsection

@section('scripting...')
<script>
</script>
@endsection




{{-- <div class="loader-bubble loader-bubble-primary m-5"></div> --}}



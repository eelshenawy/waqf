@extends('client::layouts.master')


@section('content')
    <section class="heading">
        <div class="container">
            <div class="heading__body old-requests">
                <h1>الطلبات السابقة</h1>
                <span class="line"></span>
                <p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل
                    الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم
                    لأنها
                    تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدا</p>
            </div>
        </div>
    </section>

    <section class="about__-__us__galary-taps">
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                       aria-controls="home" aria-selected="true">طلبات الايجار/البيع السابقة</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                       aria-controls="profile" aria-selected="false">طلبات الصيانة السابقة</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active myTabContent__-__new" id="home" role="tabpanel"
                     aria-labelledby="home-tab">
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">
                                            <button class="btn onhold">طلبك قيد الانتظار</button>
                                            <!-- <button class="btn agree">تمت الموافقة</button> -->
                                            <!-- <button class="btn delete">تم الرفض</button> -->
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">

                                            <button class="btn agree">تمت الموافقة</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">

                                            <button class="btn agree">تمت الموافقة</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade myTabContent__-__new" id="profile" role="tabpanel"
                     aria-labelledby="profile-tab">
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">

                                            <button class="btn agree">تمت الموافقة</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">

                                            <button class="btn agree">تمت الموافقة</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="search-results_details">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="profile_pic">
                                    <img src="img/Saratogafarms.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-9">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <h2>اسم العقار <span> تمليك</span></h2>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-1">
                                        <div class="search-results__action">

                                            <button class="btn delete">تم الرفض</button>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المدينة: الرياض - السعودية</li>
                                                    <li>السعر: 2000 ريال/شهريا</li>
                                                    <li>اسم المستأجر: محمد على</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">
                                                <ul>
                                                    <li>المبنى: الحرية</li>
                                                    <li>الدور: الثالث</li>
                                                    <li>الشقة: 8</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="search_info">
                                            <div class="search_info__details">

                                                <ul>
                                                    <li>الجوال: 0000000000</li>
                                                    <li>التاريخ: <span>12:00 PM</span> <span>28/02/2019</span></li>
                                                    <li>البريد: iddma@gmail.com</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="search_info">
                                    <div class="search_info__details">
                                        <ul>
                                            <li>المدينة: <P>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء
                                                    لصفحة
                                                    ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص</P>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
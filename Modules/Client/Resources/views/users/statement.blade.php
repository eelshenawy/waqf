@extends('client::layouts.master')

@section('content')

@section('content')
    <section class="roles-and-permissions">
        <div class="container">
            @include('admin::layouts.partials._success')

            <section class="heading">
                <div class="container">
                    <div class="heading__body old-requests">
                        <h1>  {{ __('shared::actions.statement_account') }}</h1>
                        <span class="line"></span>
                    </div>
                </div>
            </section>

            <div class="search-results_details" id="advance_added">
                <div class="row">
                    <div class="col-md-12">
                        <div class="search_info">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="search-results__action">

                                        <div class="table-responsive">
                                            <table class="display table table-striped table-bordered dataTable" id="alternative_pagination_table" style="width:100%">
                                                <caption>
                                                    {{-- {{ $vouchers->count() }} --}}
                                                </caption>
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">رقم السند </th>
                                                    <th scope="col">نوع الدفع</th>
                                                    <th scope="col">المبلغ المدفوع</th>
                                                    <th scope="col">تاريخ السند </th>
                                                    <th scope="col"> نوع الحساب </th>
                                                </tr>
                                                </thead>
                                                <tbody class="text-center">
                                                @php
                                                $sum=0;
                                                @endphp
                                                @foreach($vouchers as $key => $voucher)
                                                    @php

                                                     if($voucher->document_type == "Receipt"){
                                                        $sum +=$voucher->paid_amount;
                                                        }else{
                                                           $sum -=$voucher->paid_amount;
                                                        }
                                                        $fund=\Modules\Accounting\Entities\Fund::with('account')->find($voucher->paymentable_id);
                                                    @endphp

                                                    <tr>
                                                        <th scope="row">{{++$key}}</th>
                                                        <td>{{$voucher->number}}</td>
                                                        <td>{{$voucher->document_type == "Receipt" ? "قبض" : "صرف" }}</td>
                                                        <td>{{$voucher->paid_amount}}</td>
                                                        <td>{{$voucher->date }}</td>
                                                        <td>{{$voucher->paymentable_type == "Bank" ? "بنك" : "الصندوق"  }}</td>

                                                    </tr>

                                                @endforeach
                                                <tr>
                                                     <td scope="col"> الرصيد</td>
                                                   <td colspan="5" scope="col"> {{$sum}}</td>
                                                </tr>

                                                </tbody>
                                            </table>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection
@section('scripting...')
<script>
</script>
@endsection




{{-- <div class="loader-bubble loader-bubble-primary m-5"></div> --}}



<?php

namespace Modules\Client\Http\Controllers\User\Beneficiary;

use App\Advance;
use App\User;
use App\Repositories\User\Beneficiary\BeneficiaryRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Modules\Accounting\Entities\Voucher;
use Modules\Client\Http\Requests\StoreAdvance;
use Modules\Client\Http\Requests\StoreBeneficiary;
use mysql_xdevapi\Exception;

class BeneficiaryAdvanceController extends Controller
{

    public function __construct()
    {

    }


    public function index()
    {
        if(Auth::guard('beneficiary')->check()){
            return view('client::users.advance',['Advance'=>Advance::where('beneficiary_id',Auth::guard('beneficiary')->user()->id)->orderBy('id', 'DESC')->get()]);
        } else{
            return redirect('.login');
        }
    }

    public function store_advance(Request $request)
    {
        $data=[];
        $validator = \Validator::make($request->all(), [
            'd' => 'required|after:today'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => 'يجب ان يكون التاريخ بعد هذا التاريخ', 'status' => 400], 200);
          }else{
            $check_advance= Advance::where('beneficiary_id',\Auth::guard('beneficiary')->user()->id)->where(['is_accepted'=>0,'is_done'=>0])->get();
            $check_advance1= Advance::where('beneficiary_id',\Auth::guard('beneficiary')->user()->id)->where(['is_accepted'=>1,'is_done'=>1])->get();
            $check_advance2= Advance::where('beneficiary_id',\Auth::guard('beneficiary')->user()->id)->where(['is_accepted'=>1,'is_done'=>0])->get();
            $check_advance3= Advance::where('beneficiary_id',\Auth::guard('beneficiary')->user()->id)->where(['is_accepted'=>2,'is_done'=>0])->get();
            if($check_advance->isEmpty() && $check_advance2->isEmpty()&& $check_advance1->isNotEmpty() && $check_advance3->isNotEmpty() || $check_advance->isEmpty() && $check_advance2->isEmpty()&& $check_advance1->isEmpty() && $check_advance3->isEmpty()){
                $date = new \DateTime($request['order_datepicker_']);
                $date_pay = $date->format('Y-m-d');
                $Advance = Advance::create([
                    'amount' => isset($request['number_advance']) ? $request['number_advance'] : null,
                    'reason_advance' => isset($request['reason_advance']) ? $request['reason_advance'] : null,
                    'date_pay' => $date_pay ? $date_pay : null,
                    'number_advance' => Str::random(8),
                    'beneficiary_id' => \Auth::guard('beneficiary')->user()->id,
                ]);
                return response()->json(['success'=>'تم بالنجاح الطلب']);
            }else{
                return response()->json(['errors' => 'لم يتم قفل السلفه بعد', 'status' => 400], 200);
            }

        }

    }


    public function statement_account(Request $request){
        if(Auth::guard('beneficiary')->check()){
            return view('client::users.statement',['vouchers'=>Voucher::where('voucherable_type','سند داخلى')->where('liable_id',Auth::guard('beneficiary')->user()->id)->orderBy('id', 'DESC')->get()]);
        } else{
            return redirect('.login');
        }

    }
}

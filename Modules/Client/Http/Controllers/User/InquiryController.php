<?php

namespace Modules\Client\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Client\Http\Requests\StoreTenant;
use App\Inquiry;
class InquiryController extends Controller
{


    public function add_inquiry(Request $request)
    {
        $inquiry= new  Inquiry;
        $inquiry->name = $request->name;
        $inquiry->mobile = $request->phone;
        $inquiry->inquiry = $request->inquiry;
        $inquiry->save();
        return response()->json(['status'=>'true']);
    }


}

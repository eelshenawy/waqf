<?php

namespace Modules\Accounting\Http\Controllers\Journal;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Accounting\Entities\Journal;

class JournalController extends Controller
{

    public function index()
    {
        bread_crumb('قيود اليوميه');
        $journals = Journal::with('account')->get();
        return view('accounting::journals.index', compact('journals'));
    }


    public function create()
    {
        return view('accounting::journals.create');
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('accounting::show');
    }


    public function edit($id)
    {
        return view('accounting::edit');
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}

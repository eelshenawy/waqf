<?php

namespace Modules\Admin\Http\Controllers\Estates\Buildings;

use App\Repositories\Estate\Building\BuildingRepositoryInterface;
use Illuminate\Http\Request;
use Modules\Admin\Http\Controllers\Estates\EstateController;
use Modules\Admin\Http\Requests\StoreBuilding;
use Modules\Admin\Http\Requests\UpdateBuilding;

class BuildingController extends EstateController
{
    protected $building;

    public function __construct(BuildingRepositoryInterface $building)
    {
        $this->building = $building;
    }

    public function index()
    {
        bread_crumb('Building');
        return view('admin::estates.buildings.index', ['buildings' => $this->building->all()]);
    }

    public function create()
    {
        bread_crumb('Building | New');
        return view('admin::estates.buildings.create');
    }

    public function store(StoreBuilding $request)
    {
        $request->validated();
        $this->building->create($request->except(['_token']));
        return redirect()->route('Admin::buildings.index');
    }

    public function edit($id)
    {
        bread_crumb('Building | Edit');
        return view('admin::estates.buildings.edit', ['building' => $this->building->getWithImages($id)]);
    }

    public function update($id, UpdateBuilding $request)
    {
        $request->validated();
        $this->building->update($id, $request->except(['_token']));
        return redirect()->route('Admin::buildings.index');
    }

    public function destroy($id)
    {
        $this->building->trashing($id);
        return back();
    }




}

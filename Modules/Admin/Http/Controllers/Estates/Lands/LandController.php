<?php

namespace Modules\Admin\Http\Controllers\Estates\Lands;

use App\Repositories\Estate\Land\LandRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Admin\Http\Controllers\Estates\EstateController;
use Modules\Admin\Http\Requests\StoreLand;
use Modules\Admin\Http\Requests\UpdateLand;
use Modules\Admin\Http\Requests\UpdateBuilding;

class LandController extends EstateController
{

    protected $land;

    public function __construct(LandRepositoryInterface $land)
    {
        $this->land = $land;
    }

    public function index()
    {
        bread_crumb('Estates | Lands');
        return view('admin::estates.lands.index', ['lands' => $this->land->all()]);
    }

    public function show($id)
    {
        bread_crumb('Estates | Lands | ' . $this->land->getById($id)->land_number);
        return view('admin::estates.lands.index', ['land' => $this->land->getById($id)]);
    }

    public function create()
    {
        bread_crumb('Estates | Lands | New');
        return view('admin::estates.lands.create');
    }

    public function store(StoreLand $request)
    {
        $this->land->create($request->except(['_token']));
        session()->flash('success');
        return redirect()->route('Admin::lands.index');
    }

    public function edit($id)
    {
        bread_crumb('lands | Edit');
        return view('admin::estates.lands.edit', ['lands' => $this->land->getById($id)]);
    }

    public function update($id, UpdateLand $request)
    {
        $request->validated();
        $this->land->update($id, $request->except(['_token']));
        return redirect()->route('Admin::lands.index');
    }
    public function delete($id)
    {
        $this->land->trash($id);
        session()->flash('success');
        return back();
    }


}
